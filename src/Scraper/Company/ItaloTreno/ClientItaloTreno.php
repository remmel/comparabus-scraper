<?php

namespace Scraper\Company\ItaloTreno;

use DateTime;
use Scraper\IScraper;
use Scraper\OneWayScraper;
use Scraper\ScraperUtil;
use Scraper\TripScraper;

/**
 * Description of Scraper
 *
 * @author remymellet
 */
class ClientItaloTreno extends OneWayScraper implements IScraper{
    
    /**
     * https://biglietti.italotreno.it/Booking_Acquisto_Ricerca.aspx?Culture=fr-FR&BookingRicercaRestylingBookingAcquistoRicercaView$DropDownListPassengerType_ADT=1&BookingRicercaRestylingBookingAcquistoRicercaView$DropDownListSearchBy=columnView&BookingRicercaRestylingBookingAcquistoRicercaView$RadioButtonMarketStructure=OneWay&BookingRicercaRestylingBookingAcquistoRicercaView$TextBoxMarketOrigin1=RMT&BookingRicercaRestylingBookingAcquistoRicercaView$TextBoxMarketDestination1=SMN&BookingRicercaRestylingBookingAcquistoRicercaView$DropDownListMarketDay1=30&BookingRicercaRestylingBookingAcquistoRicercaView$DropDownListMarketDay2=&BookingRicercaRestylingBookingAcquistoRicercaView$DropDownListMarketMonth1=3-2018&BookingRicercaRestylingBookingAcquistoRicercaView$DropDownListMarketMonth2=&__EVENTARGUMENT&__EVENTTARGET=BookingRicercaRestylingBookingAcquistoRicercaView$ButtonSubmit&BookingRicercaRestylingBookingAcquistoRicercaView$ButtonSubmit=PROSEGUI?utm_medium=affiliation&utm_source=zanox&utm_content=direct&utm_campaign=traffic_building&wtk=affiliation.zanox.traffic_building
     */
    public function fetch($depStopCode, $arrStopCode, $dateFromISO){
        $d = new DateTime($dateFromISO);

        $url = 'https://biglietti.italotreno.it/Booking_Acquisto_Ricerca.aspx?' . http_build_query([
            'BookingRicercaRestylingBookingAcquistoRicercaView$DropDownListPassengerType_ADT'=> '1',
            'BookingRicercaRestylingBookingAcquistoRicercaView$DropDownListSearchBy'=> 'columnView',
            'BookingRicercaRestylingBookingAcquistoRicercaView$RadioButtonMarketStructure'=> 'OneWay',
            'BookingRicercaRestylingBookingAcquistoRicercaView$TextBoxMarketOrigin1'=> $depStopCode,
            'BookingRicercaRestylingBookingAcquistoRicercaView$TextBoxMarketDestination1'=> $arrStopCode,
            'BookingRicercaRestylingBookingAcquistoRicercaView$DropDownListMarketDay1'=> $d->format('d'),
            'BookingRicercaRestylingBookingAcquistoRicercaView$DropDownListMarketDay2'=> '',
            'BookingRicercaRestylingBookingAcquistoRicercaView$DropDownListMarketMonth1'=> $d->format('m-Y'),
            'BookingRicercaRestylingBookingAcquistoRicercaView$DropDownListMarketMonth2'=> '',
            '__EVENTARGUMENT'=> null,
            '__EVENTTARGET'=> 'BookingRicercaRestylingBookingAcquistoRicercaView$ButtonSubmit',
        ]);

        $tmpfile = tempnam (sys_get_temp_dir(), "curlcookie_italotreno");
        $data = ScraperUtil::curl([
            CURLOPT_URL => $url,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_COOKIEJAR => $tmpfile,
            CURLOPT_COOKIEFILE => $tmpfile
        ]);
        fclose($tmpfile);
        unlink($tmpfile);

        return $data;
    }

    /**
     * Parses html and create objects Price
     * Parse the content sent from the server to generate structured data
     * @return TripScraper[]
     */
    public function parse($html, $unused) {
        $xpath = ScraperUtil::createDOMXPath($html);
        $schedules = [];

        $nodeList = $xpath->query('//div[contains(@class,"item-treno")]');
        if($nodeList->length > 0) {
            $dateDMY = $xpath->query("//li[@data-search-date][@class='selected']")->item(0)->getAttribute('data-search-date');
            $dateISO = DateTime::createFromFormat("d/m/Y", $dateDMY)->format('Y-m-d');
            foreach ($nodeList as $curNode) {
                $p = $this->extractOne($curNode, $xpath, $dateISO);
                $schedules[] = $p;
            }
        }
        return $schedules;
    }

    private function extractOne(\DOMElement $el, \DOMXPath $xpath, $dateISO){
        $p = new TripScraper();

        $deparr = $xpath->query('.//div[contains(@class,"3/12")]/p', $el)->item(0)->nodeValue;
        list($dep, $arr) = explode(' > ', $deparr);

        $p->setDepDatetime(new DateTime($dateISO.' '.$dep));
        $p->setArrDatetime(new DateTime($dateISO.' '.$arr));

        //durata
        $duration = $xpath->query('.//p[@class="durata"]', $el)->item(0)->nodeValue;
        $duration = ScraperUtil::parseDurationInMinutes($duration);
        $p->setDuration($duration);

        $cambioNode = $xpath->query('.//p[@class="cambio"]', $el);
        if($cambioNode->length == 1) {
            $cambioArr = explode(' ', $cambioNode->item(0)->nodeValue);
            $p->setConnection(intval($cambioArr[0]));
        } else {
            $p->setConnection(0);
        }

        //get arr from duration
        $stazioneNode = $xpath->query('.//p[@class="stazione"]', $el);
        $p->setDepStopName($stazioneNode->item(0)->nodeValue);
        $p->setArrStopName($stazioneNode->item(1)->nodeValue);

        $cents = $xpath->query('.//p[@class="price"]/span', $el)->item(0)->nodeValue;
        $cents = str_replace(' €', '', $cents);
        $cents = floatval(str_replace(',', '.', $cents))*100;
        $p->setCents($cents);
        $p->setCurrency('EUR');
        $p->setType(TripScraper::TYPE_TRAIN);
        return $p;
    }
}
