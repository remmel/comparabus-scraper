<?php

namespace Scraper;
/**
 * Description of PriceWS
 *
 * @author remmel
 */
class TripScraper {
    const DATEHMS_ISO_PATTERN = 'Y-m-d H:i:s';
    const DATEHM_ISO_PATTERN = 'Y-m-d H:i';
    const DATE_ISO_PATTERN = 'Y-m-d';
    
    /**
     * Departure datetime
     * @var \DateTime
     */
    private $depDatetime;
    /**
     * Arrival datetime
     * @var \DateTime
     */
    private $arrDatetime;
    
    /**
     * Duration in seconds
     * @var int 
     */
    private $duration;
    
    /**
     * Price in cents of the current currency
     * @var int 
     */
    private $cents;
    
    /**
     * Currency in iso4217 format
     * eg: USD, EUR, GBP...
     * @var string 
     */
    private $currency;
    
    /**
     * Departure stop name
     * @var string
     */
    private $depStopName;

    /**
     * Arrival stop name
     * @var string
     */
    private $arrStopName;
    
    /**
     * No more space available on that trip
     * @var boolean
     */
    private $full;
    
    /**
     * Direction 0=outbound 1=inbound, 2=roundTrip outbound, 3=roundTrip inbound
     * @var int
     */
    private $direction = self::DIRECTION_OUTBOUND;
    const DIRECTION_OUTBOUND = 0;
    const DIRECTION_INBOUND = 1;
    
    /**
     * If needed for the redirection, id of the trip or link to the booking page
     * @var string
     */
    private $link;

    /**
     * Name of the company transporting the passengers
     * @var string
     */
    private $carrierName;

    /**
     * Code of the company transporting the passengers
     * @var string
     */
    private $carrierCode;

    /**
     * Custom info from the trip
     * @var string
     */
    private $info;

    /**
     * Nb of connection. 0 means direct trip. 1 means 1 connexion. null means no idea
     * @var int
     */
    private $connection;

    /**
     * Kind of transportation : avion / bus / train / carpooling / flight
     */
    private $type;
    const TYPE_AVION = 'avion';
    const TYPE_BUS = 'bus';
    const TYPE_FLIGHT = 'flight';
    const TYPE_TRAIN = 'train';
    const TYPE_CARPOOLING = 'carpooling';

    public function getDuration() {
        return $this->duration;
    }

    public function setDuration($duration) {
        $this->duration = $duration;
    }

    /**
     * @return \DateTime
     */
    public function getDepDatetime() {
        return $this->depDatetime;
    }

    public function getDepDatetimeIso() {
        return $this->depDatetime->format(self::DATEHMS_ISO_PATTERN);
    }

    public function getDepDateIso() {
        return $this->depDatetime->format(self::DATE_ISO_PATTERN);
    }

    /**
     * @param \DateTime $depDatetime
     */
    public function setDepDatetime(\DateTime $depDatetime) {
        $this->depDatetime = $depDatetime;
    }

    /**
     * @return \DateTime
     */
    public function getArrDatetime() {
        return $this->arrDatetime;
    }

    public function getArrDatetimeIso() {
        return $this->arrDatetime->format(self::DATEHMS_ISO_PATTERN);
    }

    /**
     * @param \DateTime $arrDatetime
     */
    public function setArrDatetime(\DateTime $arrDatetime) {
        $this->arrDatetime = $arrDatetime;
    }

    public function setDurationFromDateinterval(\DateInterval $di) {
        $this->duration = ScraperUtil::dateIntervalInMinutes($di);
    }

    public function getCents() {
        return $this->cents;
    }

    public function setCents($cents) {
        $this->cents = $cents;
    }

    public function getCurrency() {
        return $this->currency;
    }

    public function setCurrency($currency) {
        $this->currency = $currency;
    }

    public function getStopExtDep() {
        return $this->stopExtDep;
    }

    public function setStopExtDep($stopExtDep) {
        $this->stopExtDep = $stopExtDep;
    }

    public function getStopExtArr() {
        return $this->stopExtArr;
    }

    public function setStopExtArr($stopExtArr) {
        $this->stopExtArr = $stopExtArr;
    }
    
    public function getFull() {
        return $this->full;
    }

    public function setFull($full) {
        $this->full = $full;
    }
    
    public function getDirection() {
        return $this->direction;
    }

    public function setDirection($direction) {
        $this->direction = $direction;
    }
    
    public function getLink() {
        return $this->link;
    }

    public function setLink($link) {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getDepStopName() {
        return $this->depStopName;
    }

    /**
     * @param string $depStopName
     */
    public function setDepStopName($depStopName) {
        $this->depStopName = $depStopName;
    }

    /**
     * @return string
     */
    public function getArrStopName() {
        return $this->arrStopName;
    }

    /**
     * @param string $arrStopName
     */
    public function setArrStopName($arrStopName) {
        $this->arrStopName = $arrStopName;
    }

    /**
     * @return string
     */
    public function getCarrierName() {
        return $this->carrierName;
    }

    /**
     * @param string $carrierName
     */
    public function setCarrierName($carrierName) {
        $this->carrierName = $carrierName;
    }

    /**
     * @return string
     */
    public function getCarrierCode() {
        return $this->carrierCode;
    }

    /**
     * @param string $carrierCode
     */
    public function setCarrierCode($carrierCode) {
        $this->carrierCode = $carrierCode;
    }

    /**
     * @return string
     */
    public function getInfo() {
        return $this->info;
    }

    /**
     * @param string $info
     */
    public function setInfo($info) {
        $this->info = $info;
    }

    /**
     * @return int
     */
    public function getConnection() {
        return $this->connection;
    }

    /**
     * @param mixed $connection
     */
    public function setConnection($connection) {
        $this->connection = $connection;
    }

    /**
     * @return mixed
     */
    public function getType(){
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type){
        $this->type = $type;
    }

    public function getKey() {
        return $this->getDirection().'_'.$this->getDepDatetimeIso().'_'.$this->getArrDatetimeIso();
    }
}