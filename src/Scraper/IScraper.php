<?php
/**
 * User: remmel
 * Date: 12/5/15
 * Time: 1:23 PM
 */

namespace Scraper;

interface IScraper {
    /**
     * @return TripScraper[]
     */
    public function getPrice($departure, $arrival, $dateFromIso, $dateToIso);
}