<?php
/**
 * User: remmel
 * Date: 1/4/15
 * Time: 11:31 PM
 */

namespace Scraper;

use DateInterval;
use DateTime;
use Exception;

class ScraperUtil{
    const DATETIME_ISO_PATTERN = 'Y-m-d H:i';
    const TIMEOUT_S = 15;

    const STOPS_CSV_HEADER = [
        'code', //mandatory - stop/city code in the carrier website
        'name', //mandatory - stop name (usually a city name), in the carrier website
        'country', //if available - ISO country code
        'latitude', //if available
        'longitude' //if available
    ];

    const ROUTES_CSV_HEADER = [
        'stop_dep_code', //departure/origin stop/city code in the carrier website
        'stop_arr_code' //arrival/destination stop/city code in the carrier website
    ];
    /**
     * @param $str string eg 27,30 €
     * @return integer price in cents eg 2730
     */
    public static function getPrice($str){
        $str = str_replace(['€', '£'], '', $str);
        $str = str_replace(",", ".", $str);
        $str = trim($str);
        return floatval($str) * 100;
    }

    /**
     * Calculates the number of minutes between two datetime
     * return is always > 0
     */
    public static function calculateDurationObj(DateTime $dep, DateTime $arr) {
        $interval = $dep->diff($arr);
        return self::dateIntervalInMinutes($interval);
    }

    public static function dateIntervalInMinutes(\DateInterval $interval) {
        return $interval->days * 24 * 60 + $interval->h * 60 + $interval->i;
    }

    public static function dateIntervalInSeconds(\DateInterval $interval) {
        return $interval->days * 24 * 60 * 60 + $interval->h * 60 *60 + $interval->i *60 + $interval->s;
    }

    //TODO must have the dep/arr country to handle timezone
    public static function calculateDuration($dep, $arr){
        if(is_string($dep)) {
            $dep = new \DateTime($dep);
            $arr = new \DateTime($arr);
        }
        return self::calculateDurationObj($dep, $arr);
    }

    public static function calculateDurationTrip(TripScraper $p) {
        return self::calculateDuration($p->getDepDatetime(), $p->getArrDatetime());
    }


    /**
     * Calculates the number of minutes between two datetime using the timezone of the different countries
     * @return int
     */
    public static function calculateDurationTz($depCountry, $arrCountry, $depDatetime, $arrDatetime) {
        $start_date = new \DateTime($depDatetime);
        $since_start = $start_date->diff(new \DateTime($arrDatetime));

        $mn = $since_start->days * 24 * 60;
        $mn += ($since_start->h) * 60;
        $mn += $since_start->i;
        $mn = $since_start->invert ? -$mn : $mn;


        $mn -= self::calculateTzDiff($depCountry, $arrCountry) * 60;

        return $mn;
    }

    /**
     * Calculates the differences time between two country. eg: FR->GB:-1
     * At the same time : FR=13h GB=12h
     * @param $depCountry
     * @param $arrCountry
     * @return string
     */
    public static function calculateTzDiff($depCountry, $arrCountry) {
        //timezone of countries not in CET UTC+1
        $tz = [
            'GB' => 0,
            'MA' => 0,
            'PT' => 0
        ];
        $depTz = isset($tz[$depCountry]) ? $tz[$depCountry] : '1';
        $arrTz = isset($tz[$arrCountry]) ? $tz[$arrCountry] : '1';
        return $arrTz - $depTz;
    }

    /**
     * @param $depDatetime DateTime
     * @param $duration integer
     * @return DateTime
     */
    public static function calculateArrivalFromDepartureAndDuration(DateTime $depDatetime, $duration) {
        $depDatetime->add( new DateInterval( 'PT' . $duration . 'M' ) );
        return $depDatetime;
    }

    /**
     * Converts the hh:mm to number of minutes
     * eg: 1:25 => 60*1+25 => 85
     */
    public static function parseDurationInMinutes($string, $delimiter = ":") {
        $array = explode($delimiter, $string);
        return $array[0] * 60 + (count($array)==2?$array[1]:0);
    }

    /**
     * Convert YYYY-mm-dd into timestamp
     */
    public static function convertDateToTimestamp($isoDate){
        $d = DateTime::createFromFormat('Y-m-d', $isoDate);
        echo $d->getTimestamp();
    }

    /**
     * Converts YYYY-mm-dd date into dd/mm/yyyy
     * @param $isoDate
     * @param $pattern string d/m/Y or d-m-Y
     * @return string
     */
    public static function convertDate($isoDate, $pattern){
        return date($pattern, strtotime($isoDate));
    }

    public static function curl_retry($options, $time=0) {
        sleep($time);
        try{
            return self::curl($options);
        }catch (\Exception $e){
            return self::curl_retry($options, ($time+1)*2);
        }
    }

    /**
     * Curl wrapper
     * @param $options array eg [CURLOPT_URL => $url]
     * @return string
     * @throws Exception
     */
    public static function curl($options){
        //add default options to
        $options = $options + [
                CURLOPT_TIMEOUT => self::TIMEOUT_S,
                CURLOPT_RETURNTRANSFER => true
            ];

        $ch=curl_init();
        if(empty($ch)){
            throw new Exception("curl_init ERROR : cURL doesn't seems to be available");
        }
        curl_setopt_array($ch,$options);
        $content=curl_exec($ch);
        if(curl_errno($ch)){
            throw new Exception("curl_exec ERROR ".curl_errno($ch)." : ".curl_error($ch).'. Url: '.$options[CURLOPT_URL]);
        }
        curl_close($ch);

        if(strlen($content) == 0) {
            throw new Exception("no data fetched");
        }

        return $content;
    }

    /**
     * Extracts string located between two string
     */
    public static function get_string_between($string, $start, $end){
        $string = " ".$string;
        $ini = strpos($string,$start);
        if ($ini == 0) return "";
        $ini += strlen($start);
        $len = strpos($string,$end,$ini) - $ini;
        return substr($string,$ini,$len);
    }

    /**
     * Extract string located after string
     */
    public static function get_string_after($string, $start){
        return substr($string, strpos($string, $start) + strlen($start));
    }

    /**
     * Loads, parse dom and returns xpath object
     * /!\ in some case, need to force utf8 by adding '<?xml encoding="utf-8" ?>'.$html
     * @return \DOMXPath
     */
    public static function createDOMXPath($html, $forceutf8 = false){
        if($forceutf8) $html='<?xml encoding="utf-8" ?>'.$html;
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $xpath = new \DOMXPath($dom);
        return $xpath;
    }

    public static function extractOptionsFromHtmlAndName($html, $name) {
        $xpath = ScraperUtil::createDOMXPath($html);
        $nodeList = $xpath->query("//select[@name='$name']/option");

        $options = [];

        /** @var \DOMElement $option */
        foreach ($nodeList as $option) {
            $label = $option->nodeValue;
            $value = $option->getAttribute('value');
            $options[$value] = $label;
        }
        return $options;
    }

    public static function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
    }

    public static function endsWith($haystack, $needle) {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
    }

    public static function contains(&$haystack, $needle, $offset =0){
        return strpos($haystack, $needle, $offset) !== FALSE;
    }

    //get substring after last string
    public static function afterLast($haystack, $needle) {
        return substr($haystack, strrpos($haystack, $needle) + 1);
    }

    public static function prettyJson($json_ugly, $use2spaceInsteadOf4 = false) {
        $jsontxt4space = json_encode(json_decode($json_ugly), JSON_PRETTY_PRINT);

        if($use2spaceInsteadOf4) {
            return preg_replace_callback ('/^ +/m', function ($m) {
                return str_repeat (' ', strlen ($m[0]) / 2);
            }, $jsontxt4space);
        } else {
            return $jsontxt4space;
        }
    }

    /**
     * Format / beautiful xml
     * @param $uglyXml
     * @return string
     */
    public static function prettyXml($uglyXml) {
        $dom = new \DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($uglyXml);
        return $dom->saveXML();
    }

    /**
     * Remove duplicated trip
     * @param $trips TripScraper[]
     * @return TripScraper[]
     */
    public static function filterTrips($trips) {
        $hashKey = [];
        foreach($trips as $t) {
            $hashKey[$t->getKey()] = $t;
        }
        return array_values($hashKey);
    }

    /**
     * Remove duplicated trip keeping cheapest
     * @param $trips TripScraper[]
     * @return TripScraper[]
     */
    public static function filterTripsKeepCheapest($trips) {
        $hashKey = [];
        foreach($trips as $t) {
            $key = $t->getKey();

            if(array_key_exists($key, $hashKey)) {
                $oldT = $hashKey[$key];
                if ($oldT->getCents() < $t->getCents()) {
                    $t = $oldT;
                }
            }
            $hashKey[$key] = $t;
        }
        return array_values($hashKey);
    }

    /**
     * Keeps only trip for the selected dates
     * @param $trips TripScraper[]
     * @param $dates array dates in iso format YYYY-MM-DD
     * @return TripScraper[]
     */
    public static function filterTripsDates($trips, array $dates) {
        $tripsFiltered = [];
        foreach($trips as $trip) {
            $date = $trip->getDepDateIso();
            if(in_array($date, $dates ))
                $tripsFiltered[] = $trip;
        }
        return $tripsFiltered;
    }

    /**
     * Sometime some code return an object of an array of that object (eg if an xml node has one node or many nodes)
     * We need to take care that we always have an array
     */
    public static function addInArray($e) {
        return is_array($e) ? $e : [$e];
    }

    public static function parseFloat($string) {
        return floatval(str_replace(',', '.', $string));
    }

    /**
     * Get the csv file as array. To skip header use array_shift($rows)
     * use str_getcsv if the parameter is string (data)
     */
    public static function file_get_contents_csv($path, $delimiter = ',') {
        $rows = [];
        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, null, $delimiter)) !== FALSE) {
                $rows[]= $data;
            }
            fclose($handle);
        }
        return $rows;
    }

    /**
     * Transforms a php array in csv file
     * @param $path of the file
     */
    public static function file_put_contents_csv(array $rows, $path, $delimiter = ',') {
        $fp = fopen($path, 'w');
        foreach ($rows as $row) {
            fputcsv($fp, $row, $delimiter);
        }
        fclose($fp);
    }
}