<?php
/**
 * User: remmel
 * Date: 3/24/15
 * Time: 8:19 PM
 */

namespace Scraper;

abstract class OneWayScraper implements IScraper {
    /**
     * @return TripScraper[]
     */
    public function getPrice($departure, $arrival, $dateFromIso, $dateToIso = null){
        $trips = $this->getPriceOneWay($departure, $arrival, $dateFromIso);
        if($dateToIso){ //if return date, get return prices
            $pricesInbound = $this->getPriceOneWay($arrival, $departure, $dateToIso);
            foreach($pricesInbound as $t){
                $t->setDirection(1);
            }
            $trips = array_merge($trips, $pricesInbound);
        }
        return $trips;
    }

    /**
     * @return TripScraper[]
     */
    public function getPriceOneWay($departure, $arrival, $date){
        return $this->parse($this->fetch($departure, $arrival, $date), $date);
    }

    /**
     * Fetch the html webpage or json containing the prices & schedules for a specific date and route
     * @return string html/json
     */
    abstract public function fetch($depStopCode, $arrStopCode, $date);

    /**
     * Parses and transforms the html/json previously fetched to TripScraper array
     * @param string $content html/json/xml data
     * @param string $dateIso : sometime, the result page only contains the departure/arrival time not the date
     * @return TripScraper[]
     */
    abstract public function parse($content, $dateIso);
}