## Work to do

1. Scrap the prices of a carrier website  
Create `ClientXxx` (`extends OneWayScraper`) and `ClientXxxTest` (testFetch, testParse methods).

2. Extract the stops and routes of that carrier
Create `ClientXxxTest.testGetStopsAndRoutes` methods (or two separated methods)  
See `ScraperUtil::STOPS_CSV_HEADER` and `ScraperUtil::ROUTES_CSV_HEADER` to get more information about thoses 2 files.

3. Create the redirection form to redirect directly toward carrier result page
Create `redirect.html.twig` form

Some example have been done such has `ClientItaloTreno`. The files will be create in corresponding namespace / folder : `src/Scraper/Company/Xxx` and `tests/Scraper/Company/Xxx`. Xxx has to be replaced with carrier/company name.

---


## Setup

1. Install php7 and tools  
You must have **composer**, **git** and at >= **php7.0** installed. Check you that you have php >=7.0 `php -v`  
[PHPUnit 6](https://phpunit.de/getting-started/phpunit-6.html) is also used, but installed via composer.   
- Ubuntu/Debian : `sudo apt-get install php php-curl php-xml php-mbstring composer git`  
- Windows :  [php7](https://www.jeffgeerling.com/blog/2018/installing-php-7-and-composer-on-windows-10) - [composer](https://getcomposer.org/doc/00-intro.md#installation-windows) - [git](https://gitforwindows.org/)

2. Checkout code from bitbucket  
`git clone https://remmel@bitbucket.org/remmel/comparabus-scraper.git`

3. Install dependencies  
`cd comparabus-scraper`  
`composer install`

4. Run test  
`./vendor/bin/phpunit tests`

5. Run server  
`php -S localhost:8000 -t public`  
run in you browser our tool to help checkin that the development works well : [localhost:8000](localhost:8000)

---

## Provides the code

Send the code as zip file

## Source code
We are using composer, twig and PHPUnit 6.  
Make sure you are using php7


## Common pitfalls

- `PHP Fatal error:  Class 'PHPUnit_Framework_TestCase' not found`  
With PHPUnit 6 (php7), the class is now `\PHPUnit\Framework\TestCase`

- `Error: Class 'Scraper\Company\xxx' not found`  
As we are using **psr-0**, [the namespace must match the path.](https://www.sitepoint.com/battle-autoloaders-psr-0-vs-psr-4/)

- Access the Build-in server remotely  
On the remote server : `php -S 0.0.0.0:8000 -t public`  
Get the remote ip (eg 1.2.3.4) and then on you local computer go to that ip (eg 1.2.3.4:8000)

## Pending questions

How to let phpstorm indicates that the namespace is wrong ?  
How to check that test and class is same namespace ?