<?php
/**
 * User: remmel
 * Date: 23/03/18
 * Time: 19:42
 */

namespace Scraper\Company\ItaloTreno;

use PHPUnit\Framework\TestCase;
use Scraper\ScraperUtil;

class ClientItaloTrenoTest extends TestCase {
    const BERGAMO = 'BGM';
    const BOLOGNA = 'BC_';

    function xtestFetch() {
        $client = new ClientItaloTreno();
        $data = $client->fetch(self::BERGAMO, self::BOLOGNA, "2018-05-30");
        file_put_contents(__DIR__ . '/search.html', $data);
        $this->assertGreaterThan(1000, strlen($data));
    }

    function testParse() {
        $html = file_get_contents(__DIR__ . '/search.html');
        $client = new ClientItaloTreno();
        $prices = $client->parse($html, null);

        //normal trip
        $p = $prices[0];
        $this->assertEquals('2018-05-30 08:10:00', $p->getDepDatetimeIso(), "depdatetime");
        $this->assertEquals('2018-05-30 10:47:00', $p->getArrDatetimeIso(), "arrdatetime");
        $this->assertEquals(3590, $p->getCents(), "cents 1");
        $this->assertEquals(157, $p->getDuration(), "duration");
        $this->assertEquals('Bergamo', $p->getDepStopName());
        $this->assertEquals('Bologna', $p->getArrStopName());
        $this->assertSame(1, $p->getConnection(), "connection");
    }

    function xtestGetStopsAndRoutes() {
        $html = file_get_contents("https://biglietti.italotreno.it/Booking_Acquisto_Ricerca.aspx");
        $json = ScraperUtil::get_string_between($html, 'var stationList = ', ';');
        file_put_contents(__DIR__ . '/tmp_station.json', $json);

        $stations = json_decode($json);

        $stops =  [ScraperUtil::STOPS_CSV_HEADER];
        $routes = [ScraperUtil::ROUTES_CSV_HEADER];

        foreach ($stations as $station) {
            $stops[] = [
                $station->value,
                $station->label,
                'IT',
                null,
                null
            ];

            foreach ($station->marketHash as $arr) {
                $routes[] = [
                    $station->value,
                    $arr->value
                ];
            }
        }

        ScraperUtil::file_put_contents_csv($stops, __DIR__ . '/stops.csv');
        ScraperUtil::file_put_contents_csv($routes, __DIR__ . '/routes.csv');
    }
}
