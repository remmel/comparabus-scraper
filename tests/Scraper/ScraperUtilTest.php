<?php

namespace Scraper;

use PHPUnit\Framework\TestCase;

class ScraperUtilTest extends TestCase {

    public function testGetPrice() {
        $result = ScraperUtil::getPrice('27,30 €');
        $this->assertEquals(2730, $result);
    }

    public function testDuration() {
        $result = ScraperUtil::calculateDuration('2014-01-07 10:00', '2014-01-07 18:00');
        $this->assertEquals(8*60, $result);
    }

    public function testDurationDays() {
        $result = ScraperUtil::calculateDuration('2014-01-07 10:00', '2014-01-08 18:03');
        $this->assertEquals(8*60+24*60+3, $result);
    }

    public function testDurationObject() {
        $result = ScraperUtil::calculateDuration( new \DateTime('2014-01-07 10:00'), new \DateTime('2014-01-07 10:30'));
        $this->assertEquals(30, $result);
    }

    public function testTzDiff() {
        $this->assertEquals(-1, ScraperUtil::calculateTzDiff('FR', 'GB'));
        $this->assertEquals(0, ScraperUtil::calculateTzDiff('FR', 'FR'));
        $this->assertEquals(0, ScraperUtil::calculateTzDiff('GB', 'GB'));
        $this->assertEquals(1, ScraperUtil::calculateTzDiff('GB', 'FR'));
    }

    public function testDurationTimezone(){
        $this->assertEquals(0, ScraperUtil::calculateDurationTz('FR', 'GB', '2014-01-01 13:00', '2014-01-01 12:00'));
        $this->assertEquals(0, ScraperUtil::calculateDurationTz('GB', 'FR', '2014-01-01 12:00', '2014-01-01 13:00'));

        $this->assertEquals(30, ScraperUtil::calculateDurationTz('FR', 'GB', '2014-01-01 13:00', '2014-01-01 12:30'));
        $this->assertEquals(30, ScraperUtil::calculateDurationTz('GB', 'FR', '2014-01-01 12:00', '2014-01-01 13:30'));

        $this->assertEquals(8*60, ScraperUtil::calculateDurationTz('FR', 'GB', '2014-01-01 08:30', '2014-01-01 15:30'));
        $this->assertEquals(9*60+24*60, ScraperUtil::calculateDurationTz('FR', 'GB', '2014-01-07 10:00', '2014-01-08 18:00'));
    }

    public function testParseDurationInMin(){
        $result = ScraperUtil::parseDurationInMinutes("01:45");
        $this->assertEquals(1*60+45, $result);

        $result = ScraperUtil::parseDurationInMinutes("0:45");
        $this->assertEquals(45, $result);
    }

    public function testStringBetween(){
        $fullstring = "this is my [tag]dog[/tag]";
        $parsed = ScraperUtil::get_string_between($fullstring, "[tag]", "[/tag]");
        $this->assertEquals("dog", $parsed);

        $fullstring = "https://shop.flixbus.fr/search?departureStation=4168&arrivalStation&departureCity=2015&arrivalCity=&_ga=2.16370";
        $parsed = ScraperUtil::get_string_between($fullstring, "departureStation=", "&");
        $this->assertEquals("4168", $parsed);
    }

    public function testStartsWith(){
        $this->assertTrue(ScraperUtil::startsWith("le train ne part pas", "le train"));
        $this->assertFalse(ScraperUtil::startsWith("le train ne part pas", "train"));
        $this->assertFalse(ScraperUtil::startsWith("le train ne part pas", "pas"));
    }

    public function testEndsWith(){
        $this->assertTrue(ScraperUtil::endsWith("le train ne part pas", "part pas"));
        $this->assertFalse(ScraperUtil::endsWith("le train ne part pas", "train"));
        $this->assertFalse(ScraperUtil::endsWith("le train ne part pas", "le"));
    }

    public function testFilterDates() {
        $trips = [];
        $trips[] = $this::createTripScraper(1, "2016-05-04 16:00:00");
        $trips[] = $this::createTripScraper(2, "2016-05-05 10:00:00");
        $trips[] = $this::createTripScraper(3, "2016-05-05 16:00:00");
        $trips[] = $this::createTripScraper(4, "2016-05-06 10:00:00");
        $trips[] = $this::createTripScraper(5, "2016-05-06 16:00:00");
        $trips[] = $this::createTripScraper(6, "2016-05-04 10:00:00");

        $filtered = ScraperUtil::filterTripsDates($trips, ['2016-05-05']);

        $this->assertEquals(2, count($filtered));
        $this->assertEquals(2, $filtered[0]->getCents());
        $this->assertEquals(3, $filtered[1]->getCents());

        $filtered = ScraperUtil::filterTripsDates($trips, ['2016-05-05', '2016-05-06', '2016-05-01']);

        $this->assertEquals(4, count($filtered));
        $this->assertEquals(2, $filtered[0]->getCents());
        $this->assertEquals(3, $filtered[1]->getCents());
        $this->assertEquals(4, $filtered[2]->getCents());
        $this->assertEquals(5, $filtered[3]->getCents());
    }

    protected static function createTripScraper($cents, $datetimeDepIso) {
        $t = new TripScraper();
        $t->setCents($cents);
        $t->setDepDatetime(new \DateTime($datetimeDepIso));
        return $t;
    }

    public function testGet_string_after() {
        $actual = ScraperUtil::get_string_after("www.sfsf.fr?q=abcdef", "?q=");
        $this->assertEquals("abcdef", $actual);
    }

    public function testParseFloat() {
        $this->assertEquals(11.33, ScraperUtil::parseFloat("11,33"));
    }

    public function testExtractOptions() {
        $values = ScraperUtil::extractOptionsFromHtmlAndName(
            file_get_contents(__DIR__ . '/scraperutil_options.html'),
            'DropDownList_CittaPartenza');

        $expected = [
            0 =>'Scegli la città...',
            49 => 'Bari (aeroporto)',
            11 => 'Ferrandina',
            5 => 'Francavilla'
        ];
        $this->assertEquals(json_encode($expected), json_encode($values));
    }

    //must handle number (no quote)
    public function testFile() {
        $file_contents_csv = __DIR__ . '/file_contents.csv';

        $data = ScraperUtil::file_get_contents_csv($file_contents_csv);

        $dataExpected = [
            ['name', 'age'],
            ['James', '19'],
            ['Bob', '20'],
            ['Mike Jr', '25']
        ];

        $this->assertSame($dataExpected, $data);

        $f = tempnam(sys_get_temp_dir(), 'ScraperUtil.file_get_contents_csv');
        ScraperUtil::file_put_contents_csv($data, $f);

        $txt = file_get_contents($f);
        $txtExpected = file_get_contents($file_contents_csv);
		$txtExpected = str_replace("\r\n","\n", $txtExpected); //because of windows encoding

        $this->assertEquals($txtExpected, $txt);
        unlink($f);
    }
}
