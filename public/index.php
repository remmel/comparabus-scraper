<?php

require_once __DIR__ . '/../vendor/autoload.php';

$company = isset($_GET['company']) ? $_GET['company'] : '';
$depStopCode = isset($_GET['depStopCode']) ? $_GET['depStopCode'] : '';
$arrStopCode = isset($_GET['arrStopCode']) ? $_GET['arrStopCode'] : '';
$date = isset($_GET['date']) ? $_GET['date'] : '';
$action = isset($_GET['action']) ? $_GET['action'] : '';

/** @var class $clients */
$clients = [
    \Scraper\Company\ItaloTreno\ClientItaloTreno::class,
];
$errorMsg = [];

/**
 * Retunrs
 * @param $clients
 * @param $shortName
 */
function getClientFromName($clients, $shortName) {
    foreach ($clients as $client) {
        $rf = new \ReflectionClass($client);
        if ($rf->getShortName() == $shortName) {
            return $client;
        }
    }
    return null;
}

$redirect = null;


if ($company) {
    $namespace = null;

    $clientClass = getClientFromName($clients, $company);

    if ($clientClass) {
        $namespace = (new \ReflectionClass($clientClass))->getNamespaceName();
        $stopsPath = __DIR__ . '/../tests/' . str_replace('\\', '/', $namespace) . '/stops.csv';

        if (file_exists($stopsPath)) {
            $stops = \Scraper\ScraperUtil::file_get_contents_csv($stopsPath);
            array_shift($stops);
        } else {
            $errorMsg[] = "File <em>stops.csv</em> for $company not found in <em>$stopsPath</em><br/>Thus the autocompletion for depStopCode and arrStopCode won't be populated.";
        }


        if ($depStopCode && $arrStopCode && $date) {
            switch ($action) {
                case 'search';
                    /** @var \Scraper\OneWayScraper $client */
                    $client = new $clientClass;
                    $prices = $client->getPriceOneWay($depStopCode, $arrStopCode, $date);
                    break;

                case 'redirect':
                    $redirectDir = __DIR__ . '/../src/' . str_replace('\\', '/', $namespace);
                    $redirectFile = 'redirect.html.twig';

                    if (file_exists("$redirectDir/$redirectFile")) {
                        $loader = new Twig_Loader_Filesystem($redirectDir);
                        $twig = new Twig_Environment($loader, array());

                        $redirect = $twig->render($redirectFile, [
                            'depStopCode' => $depStopCode,
                            'arrStopCode' => $arrStopCode,
                            'dateFrom' => $date
                        ]);
                    } else {
                        $errorMsg[] = "File <em>$redirectFile</em> not found in <em>$redirectDir</em><br />Thus the redirection cannot be tested.";
                    }


            }
        }
    }
} ?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="/">comparabus-scraper test tools</a>
</nav>

<main role="main" class="container" style="max-width:1000px; margin: 20px">

    <?php foreach ($errorMsg as $msg) { ?>
        <div class="alert alert-warning" role="alert"><?= $msg ?></div>
    <?php } ?>

    <form>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Company</label>
            <div class="col-sm-10">
                <select name="company" class="form-control" onchange="this.form.submit();"
                        title="list coming from index.php::$clients">
                    <option value="">-- Company class --</option>
                    <?php foreach ($clients as $client) {
                        $name = (new \ReflectionClass($client))->getShortName();
                        ?>
                        <option <?= $company == $name ? 'selected' : '' ?>><?= $name ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-3">
                <label>depStopCode</label>
                <input list="depStopCode" class="form-control" name="depStopCode" placeholder="depStopCode"
                       value="<?= $depStopCode ?>">
                <datalist id="depStopCode">
                    <?php foreach ($stops as $s) { ?>
                        <option value="<?= $s[0] ?>"><?= $s[1] ?></option>
                    <?php } ?>
                </datalist>
            </div>
            <div class="col-md-3">
                <label>arrStopCode</label>
                <input list="arrStopCode" class="form-control" name="arrStopCode" placeholder="arrStopCode"
                       value="<?= $arrStopCode ?>">
                <datalist id="arrStopCode">
                    <?php foreach ($stops as $s) { ?>
                        <option value="<?= $s[0] ?>"><?= $s[1] ?></option>
                    <?php } ?>
                </datalist>
            </div>
            <div class="col-md-4">
                <label>Date</label>
                <input type="date" class="form-control" placeholder="Password" name="date" value="<?= $date ?>">
            </div>
            <div class="col-md-1">
                <label>___</label>
                <button type="submit" class="btn btn-primary" name="action" value="search">Prices</button>
            </div>
            <div class="col-md-1">
                <label>___</label>
                <button type="submit" class="btn btn-primary" name="action" value="redirect">Redirect</button>
            </div>
        </div>
    </form>

    <?php if (isset($prices)) { ?>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Departure</th>
                <th scope="col">Arrival</th>
                <th scope="col">Price</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($prices as $k => $p) { ?>
                <tr>
                    <th><?= $k ?></th>
                    <td><?= $p->getDepDatetimeIso() ?><br/><?= $p->getDepStopName() ?></td>
                    <td><?= $p->getArrDatetimeIso() ?><br/><?= $p->getArrStopName() ?></td>
                    <td><?= $p->getCents() / 100 ?> <?= $p->getCurrency() ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } else if ($redirect) { ?>
        <div style="max-width:1000px; margin: 20px" class="container redirect">
            <?= $redirect ?>
        </div>
    <?php } ?>
</main>


<style>
    .redirect input {
        display: block;
    }
</style>